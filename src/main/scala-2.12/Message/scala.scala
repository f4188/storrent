package Message

/**
  * Created by Fawaz A on 10/3/2017.
  */

import java.nio.ByteBuffer
  import scala.math.BigInt

abstract class BitTorMsg {

  def len : Int
  def msgType : Option[Byte]

  def pack : Array[Byte] = {
    val msg = ByteBuffer.allocate(len)
    msg.putInt(0, len)
    msg.put(4, id.get)
    msg.array()
  }
}

object BitTorMsg {

  def unpack(msg : Array[Byte]) : BitTorMsg = {
    if(msg.length == 4) KeepAlive.unpack(msg)
    msg(4) match {
      case Choke.MESSAGE_TYPE => Choke.unpack(msg)
      case Unchoke.MESSAGE_TYPE => Unchoke.unpack(msg)
      case Interested.MESSAGE_TYPE => Interested.unpack(msg)
      case Uninterested.MESSAGE_TYPE => Uninterested.unpack(msg)
      case Request.MESSAGE_TYPE => Request.unpack(msg)
      case Piece.MESSAGE_TYPE => Piece.unpack(msg)
      case 

    }
  }

}

case class KeepAlive() extends BitTorMsg {

  def len : Int = KeepAlive.MESSAGE_LEN
  def msgType : Option[Byte] = KeepAlive.MESSAGE_TYPE

}

object KeepAlive {

  def MESSAGE_LEN : Int = 4
  def MESSAGE_TYPE : Option[Byte] = None
  def unpack(msg : Array[Byte]) : KeepAlive = {
    KeepAlive()
  }
}

case class Unchoke() extends BitTorMsg {

  override def len : Int = Unchoke.MESSAGE_LEN
  override def msgType : Option[Byte] = Some(Unchoke.MESSAGE_TYPE)

  override def pack : Array[Byte] = {
    val msg = ByteBuffer.allocate(len)
    msg.put(super.pack, 0, 5)
    msg.array()
  }
}

object Unchoke {

  val MESSAGE_TYPE : Byte = 0x5
  val MESSAGE_LEN : Int = 5
  def unpack(msg : Array[Byte]) : Unchoke = {
    Unchoke()
  }

}

case class Choke() extends BitTorMsg {

  override def len : Int = Choke.MESSAGE_LEN
  override def msgType : Option[Byte] = Some(Choke.MESSAGE_TYPE)

  override def pack : Array[Byte] = {
    val msg = ByteBuffer.allocate(len)
    msg.put(super.pack, 0, 5)
    msg.array()
  }

}

object Choke {

  val MESSAGE_LEN : Int = 5
  val MESSAGE_TYPE : Byte = 5

  def unpack(msg : Array[Byte]) : Choke = {
    Choke()
  }
}

case class Interested() extends BitTorMsg {

}

object Interested {

  val MESSAGE_LEN : Int = 17
  val MESSAGE_TYPE : Byte = 2

}

case class Uninterested() extends BitTorMsg {}

object UnInterested {

  val MESSAGE_LEN : Int = 17
  val MESSAGE_TYPE : Byte = 2
}

case class Bitfield() extends BitTorMsg
object Bitfield {

  val MESSAGE_LEN : Int = 17
  val MESSAGE_TYPE : Byte = 2
}

case class Have() extends BitTorMsg
object Have {

  val MESSAGE_LEN : Int = 17
  val MESSAGE_TYPE : Byte = 2
}

case class Request(index : Int, begin : Int, length : Int ) extends BitTorMsg {

  override def len : Int = Request.MESSAGE_LEN
  override def msgType : Byte = Request.MESSAGE_TYPE

}
object Request {

  val MESSAGE_LEN : Int = 17
  val MESSAGE_TYPE : Byte = 2

}

case class Piece() extends BitTorMsg


